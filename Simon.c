#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#define u8 uint8_t
#define u64 uint64_t

#define ROTL64(x,r) (((x)<<(r)) | (x>>(64-(r))))
#define ROTR64(x,r) (((x)>>(r)) | ((x)<<(64-(r))))

#define f64(x) ((ROTL64(x,1) & ROTL64(x,8)) ^ ROTL64(x,2))
#define R64x2(x,y,k1,k2) (y^=f64(x), y^=k1, x^=f64(y), x^=k2)

void Words64ToBytes(u64 words[],u8 bytes[],int numwords)
{
int i,j=0;
  for(i=0;i<numwords;i++){
    bytes[j]=(u8)words[i];
    bytes[j+1]=(u8)(words[i]>>8);
    bytes[j+2]=(u8)(words[i]>>16);
    bytes[j+3]=(u8)(words[i]>>24);
    bytes[j+4]=(u8)(words[i]>>32);
    bytes[j+5]=(u8)(words[i]>>40);
    bytes[j+6]=(u8)(words[i]>>48);
    bytes[j+7]=(u8)(words[i]>>56);
    j+=8;
} }

void BytesToWords64(u8 bytes[],u64 words[],int numbytes)
{
int i,j=0;
  for(i=0;i<numbytes/8;i++){
    words[i]=(u64)bytes[j]  | ((u64)bytes[j+1]<<8)  | ((u64)bytes[j+2]<<16) |
      ((u64)bytes[j+3]<<24) | ((u64)bytes[j+4]<<32) | ((u64)bytes[j+5]<<40) |
      ((u64)bytes[j+6]<<48) | ((u64)bytes[j+7]<<56); j+=8;
  }
}

void Simon128256KeySchedule(u64 K[],u64 rk[])
{
  u64 i,D=K[3],C=K[2],B=K[1],A=K[0];
  u64 c=0xfffffffffffffffcLL, z=0xfdc94c3a046d678bLL;
for(i=0;i<64;){
rk[i++]=A; A^=c^(z&1)^ROTR64(D,3)^ROTR64(D,4)^B^ROTR64(B,1); z>>=1;
rk[i++]=B; B^=c^(z&1)^ROTR64(A,3)^ROTR64(A,4)^C^ROTR64(C,1); z>>=1;
rk[i++]=C; C^=c^(z&1)^ROTR64(B,3)^ROTR64(B,4)^D^ROTR64(D,1); z>>=1;
rk[i++]=D; D^=c^(z&1)^ROTR64(C,3)^ROTR64(C,4)^A^ROTR64(A,1); z>>=1;
}
rk[64]=A; A^=c^0^ROTR64(D,3)^ROTR64(D,4)^B^ROTR64(B,1); rk[65]=B;
B^=c^1^ROTR64(A,3)^ROTR64(A,4)^C^ROTR64(C,1); rk[66]=C;
C^=c^0^ROTR64(B,3)^ROTR64(B,4)^D^ROTR64(D,1); rk[67]=D;
D^=c^0^ROTR64(C,3)^ROTR64(C,4)^A^ROTR64(A,1); rk[68]=A;
rk[69]=B; rk[70]=C; rk[71]=D;
}

void Simon128256Encrypt(u64 Pt[],u64 Ct[],u64 rk[])
{
u64 i;
  Ct[0]=Pt[0]; Ct[1]=Pt[1];
  for(i=0;i<72;i+=2) R64x2(Ct[1],Ct[0],rk[i],rk[i+1]);
}

void Simon128256Decrypt(u64 Pt[],u64 Ct[],u64 rk[])
{
int i;
  Pt[0]=Ct[0]; Pt[1]=Ct[1];
for(i=71;i>=0;i-=2) R64x2(Pt[0],Pt[1],rk[i],rk[i-1]);
}

int main()
{
    u8 pt[] = {0x69, 0x73, 0x20, 0x61, 0x20, 0x73, 0x69, 0x6d, 0x6f, 0x6f, 0x6d, 0x20, 0x69, 0x6e, 0x20, 0x74};
    u8 k[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f};
    u64 Pt[2];
    u64 K[4];
    BytesToWords64(pt,Pt,16);

    BytesToWords64(k,K,32);
    
    u64 rk[72];
    Simon128256KeySchedule(K, rk);
    
    u64 Ct[2];
    u8 ct[16];
    Simon128256Encrypt(Pt,Ct,rk);
    //printf("val = %lx\n", ct[0]);
    Words64ToBytes(Ct,ct,2);
    for (int i=0; i<16; i++)
        printf("%hhx", ct[i]);
    return 0;
}

