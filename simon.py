import numpy as np

def to_pt(plaintext):
    length = len(plaintext)
    pt = np.array([0]*(length//2), dtype=np.uint8)
    for i in range(0, length, 2):
        pt[i//2] = int(plaintext[i:i+2], 16)
    return pt

def to_text(numbers):
    ct = ''
    for n in numbers:
        h = hex(n)
        if len(h) < 4:
            h = '0' + h[2:]
        else:
            h = h[2:]
        ct += h
    return ct

def bytes_to_words64(bytes):
    length = len(bytes)
    words = np.array([0]*(length//8), dtype=np.uint64)
    j = 0
    for i in range(length//8):
        words[i] = (bytes[j].astype(np.uint64) | (bytes[j+1]<<8).astype(np.uint64) |
                   (bytes[j+2]<<16).astype(np.uint64)) | (bytes[j+3]<<24).astype(np.uint64) | \
                   (bytes[j+4]<<32).astype(np.uint64) | (bytes[j+5]<<40).astype(np.uint64) | \
                   (bytes[j+6]<<48).astype(np.uint64) | (bytes[j+7]<<56).astype(np.uint64)
        j += 8
    return words

def words64_to_bytes(words):
    length = len(words)
    bytes = np.array([0]*(length*8), dtype=np.uint8)
    j = 0
    for i in range(length):
        bytes[j] = words[i].astype(np.uint8)
        bytes[j+1] = (words[i] >> np.uint(8)).astype(np.uint8)
        bytes[j+2] = (words[i] >> np.uint(16)).astype(np.uint8)
        bytes[j+3] = (words[i] >> np.uint(24)).astype(np.uint8)
        bytes[j+4] = (words[i] >> np.uint(32)).astype(np.uint8)
        bytes[j+5] = (words[i] >> np.uint(40)).astype(np.uint8)
        bytes[j+6] = (words[i] >> np.uint(48)).astype(np.uint8)
        bytes[j+7] = (words[i] >> np.uint(56)).astype(np.uint8)
        j += 8
    return bytes

def rotate_l(x, s):
    return ((x<<np.uint64(s)) | (x>>np.uint64(64-s)))

def rotate_r(x, s):
    return ((x>>np.uint64(s)) | (x<<np.uint64(64-s)))

def f64(x):
    return (rotate_l(x,1) & rotate_l(x,8) ^ rotate_l(x,2))

def r64x2(x, y, k1, k2):
    y ^= f64(x)
    y ^= k1
    x ^= f64(y)
    x ^= k2
    return x, y

def key_schedule(K):
    D, C, B, A = K[3], K[2], K[1], K[0]
    c, z = 0xfffffffffffffffc, 0xfdc94c3a046d678b
    rk = np.array([0]*72, dtype=np.uint64)
    i = 0
    while i < 64:
        rk[i] = A
        i += 1
        A ^= c^(z&1)^rotate_r(D,3)^rotate_r(D,4)^B^rotate_r(B,1)
        z >>= 1
        rk[i] = B
        i += 1
        B ^= c^(z&1)^rotate_r(A,3)^rotate_r(A,4)^C^rotate_r(C,1)
        z >>= 1
        rk[i] = C
        i += 1
        C ^= c^(z&1)^rotate_r(B,3)^rotate_r(B,4)^D^rotate_r(D,1)
        z >>= 1
        rk[i] = D
        i += 1
        D ^= c^(z&1)^rotate_r(C,3)^rotate_r(C,4)^A^rotate_r(A,1)
        z >>= 1
    rk[64] = A
    A ^= c^0^rotate_r(D,3)^rotate_r(D,4)^B^rotate_r(B,1)
    rk[65] = B
    B ^= c^1^rotate_r(A,3)^rotate_r(A,4)^C^rotate_r(C,1)
    rk[66] = C
    C ^= c^0^rotate_r(B,3)^rotate_r(B,4)^D^rotate_r(D,1)
    rk[67] = D
    D ^= c^0^rotate_r(C,3)^rotate_r(C,4)^A^rotate_r(A,1)
    rk[68], rk[69], rk[70], rk[71] = A, B, C, D
    return rk

def encrypt(Pt, rk):
    Ct = np.array([0]*2, dtype=np.uint64)
    Ct[0], Ct[1] = Pt[0], Pt[1]
    i = 0
    while i < 72:
        Ct[1], Ct[0] = r64x2(Ct[1], Ct[0], rk[i], rk[i+1])
        i += 2
    return Ct

def decrypt(Ct, rk):
    Pt = np.array([0]*2, dtype=np.uint64)
    Pt[0], Pt[1] = Ct[0], Ct[1]
    i = 71
    while i >= 0:
        Pt[0], Pt[1] = r64x2(Pt[0], Pt[1], rk[i], rk[i-1])
        i -= 2
    return Pt

def test():
    plaintext = "697320612073696D6F6F6D20696E2074"
    key = '000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F'

    pt = to_pt(plaintext)
    Pt = bytes_to_words64(pt)

    k = to_pt(key)
    K = bytes_to_words64(k)

    rk = key_schedule(K)
    Ct = encrypt(Pt, rk)
    #print(hex(Ct[1]), hex(Ct[0]))
    ct = words64_to_bytes(Ct)
    text = to_text(ct)
    print('ciphertext:', text)
    De = decrypt(Ct, rk)
    print('Decryption = Pt:', De == Pt)

def calc_y():
    k0 = '0'*64
    ints = ['0'*31+'1', '0'*31+'2','0'*31 + '3']
    y_hex = []
    y = []
    for p in ints:
        Pt = bytes_to_words64(to_pt(p))
        rk = key_schedule(bytes_to_words64(to_pt(k0)))
        text = to_text(words64_to_bytes(encrypt(Pt, rk)))
        y_hex.append(text)
        y.append(int('0x'+text, 16))

    print(y_hex)
    #print(y)
    print('y1 XOR y2 = y3?', y[0] ^ y[1] == y[2])
    print('1 XOR 2 = 3?', 1^2==3)


#test()
calc_y()
